# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_07_07_095420) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "accounts", force: :cascade do |t|
    t.string "provider"
    t.string "uid"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["provider"], name: "index_accounts_on_provider"
    t.index ["uid"], name: "index_accounts_on_uid"
    t.index ["user_id"], name: "index_accounts_on_user_id"
  end

  create_table "comments", force: :cascade do |t|
    t.string "body"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "contacts", force: :cascade do |t|
    t.string "email_from", null: false
    t.string "body", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email_from"], name: "index_contacts_on_email_from"
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string "slug", null: false
    t.integer "sluggable_id", null: false
    t.string "sluggable_type", limit: 50
    t.string "scope"
    t.datetime "created_at"
    t.index ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true
    t.index ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type"
    t.index ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id"
    t.index ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type"
  end

  create_table "htmltohamls", force: :cascade do |t|
    t.integer "user_id"
    t.boolean "success", default: true
    t.boolean "system", default: true
    t.integer "lines_in"
    t.integer "symbols_in"
    t.integer "lines_out"
    t.integer "symbols_out"
    t.integer "handle_time"
    t.jsonb "error_stack", default: {}
    t.string "error_body"
    t.string "source_html"
    t.string "response_haml"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "languages", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_languages_on_name"
  end

  create_table "libraries", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_libraries_on_name"
  end

  create_table "likes", force: :cascade do |t|
    t.integer "user_id"
    t.integer "project_id"
    t.integer "comment_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "projects", force: :cascade do |t|
    t.boolean "published", default: false
    t.boolean "big", default: false
    t.string "logo_image"
    t.string "images_desktop", default: [], array: true
    t.string "images_tablet", default: [], array: true
    t.string "images_mobile", default: [], array: true
    t.string "title"
    t.string "teaser"
    t.string "description"
    t.string "body"
    t.integer "primary_language"
    t.integer "additional_languages", default: [], array: true
    t.integer "libs", default: [], array: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["additional_languages"], name: "index_projects_on_additional_languages"
    t.index ["big"], name: "index_projects_on_big"
    t.index ["libs"], name: "index_projects_on_libs"
    t.index ["primary_language"], name: "index_projects_on_primary_language"
    t.index ["published"], name: "index_projects_on_published"
    t.index ["teaser"], name: "index_projects_on_teaser"
    t.index ["title"], name: "index_projects_on_title"
  end

  create_table "subscribes", force: :cascade do |t|
    t.string "email"
    t.integer "user_id"
    t.string "type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_subscribes_on_email", unique: true
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "username", null: false
    t.string "display_name", default: "", null: false
    t.string "type", default: "Free"
    t.boolean "deleted", default: false
    t.datetime "premium_expire"
    t.string "phone_number"
    t.string "phone_confirmation_code"
    t.boolean "phone_confirmed", default: false
    t.datetime "phone_confirmed_at"
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.inet "last_sign_in_local_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["phone_number"], name: "index_users_on_phone_number", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["unlock_token"], name: "index_users_on_unlock_token", unique: true
    t.index ["username"], name: "index_users_on_username", unique: true
  end

end
