class CreateSubscribes < ActiveRecord::Migration[5.2]
  def change
    create_table :subscribes do |t|
      t.string :email, unique: true
      t.integer :user_id, require: false
      t.string :type
      t.timestamps
    end

    add_index :subscribes, :email, unique: true
  end
end
