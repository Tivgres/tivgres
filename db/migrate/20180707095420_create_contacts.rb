class CreateContacts < ActiveRecord::Migration[5.2]
  def change
    create_table :contacts do |t|
      t.string :email_from, null: false, index: true
      t.string :body, null: false
      t.timestamps
    end
  end
end
