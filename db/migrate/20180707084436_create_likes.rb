class CreateLikes < ActiveRecord::Migration[5.2]
  def change
    create_table :likes do |t|
      t.integer :user_id
      t.integer :project_id, default: nil, required: false
      t.integer :comment_id, default: nil, required: false
      t.timestamps
    end
  end
end
