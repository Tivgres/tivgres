class CreateAccounts < ActiveRecord::Migration[5.2]
  def change
    create_table :accounts do |t|
      t.string :provider, index: true
      t.string :uid, unique: true, index: true
      t.integer :user_id, index: true
      t.timestamps
    end
  end
end
