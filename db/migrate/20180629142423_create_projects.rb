# frozen_string_literal: true

class CreateProjects < ActiveRecord::Migration[5.2]
  def change
    create_table :projects do |t|
      t.boolean :published, default: false, index: true
      t.boolean :big, default: false, index: true
      t.string :logo_image
      t.string :images_desktop, array: true, default: []
      t.string :images_tablet, array: true, default: []
      t.string :images_mobile, array: true, default: []
      t.string :title, index: true
      t.string :teaser, index: true
      t.string :description
      t.string :body
      t.integer :primary_language, as: :language_id, index: true
      t.integer :additional_languages, array: true, default: [],
                                       as: :language_id, index: true
      t.integer :libs, array: true, default: [],
                       as: :library_id, index: true
      t.timestamps
    end
  end
end
