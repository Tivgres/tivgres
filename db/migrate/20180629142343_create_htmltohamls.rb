# frozen_string_literal: true

class CreateHtmltohamls < ActiveRecord::Migration[5.2]
  def change
    create_table :htmltohamls do |t|
      t.integer :user_id, default: nil
      t.boolean :success, default: true
      t.boolean :system, default: true
      t.integer :lines_in
      t.integer :symbols_in
      t.integer :lines_out
      t.integer :symbols_out
      t.integer :handle_time
      t.jsonb :error_stack, default: {}
      t.string :error_body, default: nil
      t.string :source_html, default: nil
      t.string :response_haml, default: nil
      t.timestamps
    end
  end
end
