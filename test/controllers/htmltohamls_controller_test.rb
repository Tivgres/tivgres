require 'test_helper'

class HtmltohamlsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @htmltohaml = htmltohamls(:one)
  end

  test "should get index" do
    get htmltohamls_url
    assert_response :success
  end

  test "should get new" do
    get new_htmltohaml_url
    assert_response :success
  end

  test "should create htmltohaml" do
    assert_difference('Htmltohaml.count') do
      post htmltohamls_url, params: { htmltohaml: {  } }
    end

    assert_redirected_to htmltohaml_url(Htmltohaml.last)
  end

  test "should show htmltohaml" do
    get htmltohaml_url(@htmltohaml)
    assert_response :success
  end

  test "should get edit" do
    get edit_htmltohaml_url(@htmltohaml)
    assert_response :success
  end

  test "should update htmltohaml" do
    patch htmltohaml_url(@htmltohaml), params: { htmltohaml: {  } }
    assert_redirected_to htmltohaml_url(@htmltohaml)
  end

  test "should destroy htmltohaml" do
    assert_difference('Htmltohaml.count', -1) do
      delete htmltohaml_url(@htmltohaml)
    end

    assert_redirected_to htmltohamls_url
  end
end
