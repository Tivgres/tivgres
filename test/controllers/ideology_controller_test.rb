require 'test_helper'

class IdeologyControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get ideology_index_url
    assert_response :success
  end

  test "should get new" do
    get ideology_new_url
    assert_response :success
  end

  test "should get create" do
    get ideology_create_url
    assert_response :success
  end

  test "should get edit" do
    get ideology_edit_url
    assert_response :success
  end

  test "should get update" do
    get ideology_update_url
    assert_response :success
  end

end
