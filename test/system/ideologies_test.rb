require "application_system_test_case"

class IdeologiesTest < ApplicationSystemTestCase
  setup do
    @ideology = ideologies(:one)
  end

  test "visiting the index" do
    visit ideologies_url
    assert_selector "h1", text: "Ideologies"
  end

  test "creating a Ideology" do
    visit ideologies_url
    click_on "New Ideology"

    click_on "Create Ideology"

    assert_text "Ideology was successfully created"
    click_on "Back"
  end

  test "updating a Ideology" do
    visit ideologies_url
    click_on "Edit", match: :first

    click_on "Update Ideology"

    assert_text "Ideology was successfully updated"
    click_on "Back"
  end

  test "destroying a Ideology" do
    visit ideologies_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Ideology was successfully destroyed"
  end
end
