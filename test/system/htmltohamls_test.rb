require "application_system_test_case"

class HtmltohamlsTest < ApplicationSystemTestCase
  setup do
    @htmltohaml = htmltohamls(:one)
  end

  test "visiting the index" do
    visit htmltohamls_url
    assert_selector "h1", text: "Htmltohamls"
  end

  test "creating a Htmltohaml" do
    visit htmltohamls_url
    click_on "New Htmltohaml"

    click_on "Create Htmltohaml"

    assert_text "Htmltohaml was successfully created"
    click_on "Back"
  end

  test "updating a Htmltohaml" do
    visit htmltohamls_url
    click_on "Edit", match: :first

    click_on "Update Htmltohaml"

    assert_text "Htmltohaml was successfully updated"
    click_on "Back"
  end

  test "destroying a Htmltohaml" do
    visit htmltohamls_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Htmltohaml was successfully destroyed"
  end
end
