$(document).on('turbolinks:load', function() {

    var timer, delay = 500;
    $('#contactEmailFrom').bind('keydown blur change', function(e) {
        clearTimeout(timer);
        timer = setTimeout(function() {
            checkFields();
        }, delay );
    });

    $('#contactBody').bind('keydown blur change', function(e) {
        clearTimeout(timer);
        timer = setTimeout(function() {
            checkFields();
        }, delay );
    });

    $('#contactCaptcha').bind('keydown blur change', function(e) {
        clearTimeout(timer);
        timer = setTimeout(function() {
            checkFields();
        }, delay );
    });
});

// I know, it's look like a dirty hack, but I think it's more simple.
function checkFields() {
    if (changedEmail() && changedBody() && checkTheCaptcha()){
        $('#contactSendButton').prop( "disabled", false );
    } else {
        $('#contactSendButton').prop( "disabled", true );
    }
}

function changedEmail() {
    var email = $("#contactEmailFrom").val();
    if (email.length === 0){
        $('.email-errors').removeClass('invalid-feedback').addClass("d-none").text('');
        return false;
    } else if (!validateEmail(email)) {
        $('.email-errors').addClass('invalid-feedback').text('Wrong email!');
        return false;
    } else {
        $('.email-errors').removeClass('invalid-feedback').addClass("d-none").text('');
        return true;
    }
}

function changedBody() {
    var body = $("#contactBody").val();
    if (body.length === 0){
        $('.body-errors').removeClass('invalid-feedback').addClass("d-none").text('');
        return false;
    } else if (body.length < 5) {
        $('.body-errors').addClass('invalid-feedback').text('Too short!');
        return false;
    } else {
        $('.body-errors').removeClass('invalid-feedback').addClass("d-none").text('');
        return true;
    }
}

function checkTheCaptcha() {
    var captchaField = $('#contactCaptcha');
    var captchaQuestion = captchaField.attr('placeholder');
    var captchaAnswer = parseInt(captchaField.val(), 10);
    var subQuestions = captchaQuestion.split(' + ');
    var one = subQuestions[0].split(" ");
    one = parseInt(one[one.length - 1], 10);
    var two = subQuestions[1].split(" ");
    two = parseInt(two[0], 10);
    var realAnswer = one + two;
    return (realAnswer === captchaAnswer);
}

function validateEmail(email) {
    var regularEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regularEmail.test(String(email).toLowerCase());
}