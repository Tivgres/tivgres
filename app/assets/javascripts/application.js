//= require rails-ujs
//= require activestorage
//= require turbolinks
//= require jquery3
//= require popper
//= require bootstrap
//= require particles
//= require_tree .

$(function() {
    setTimeout(function() {
        $('.notification')
            .fadeOut('slow')
            .empty();
    }, 10000);
});

$(document).on('turbolinks:load', function() {

    $("#registerButton").on( "click", function() {
        console.log( "clicked register");
        register();
    });

    $("#loginButton").on( "click", function() {
        console.log( "clicked login");
        login();
    });
});

function register() {
    console.log( "register fun");
    $('#authorizationForm').attr('action', '/register');
    sendForm();
}

function login() {
    console.log( "login fun");
    $('#authorizationForm').attr('action', '/login');
    sendForm();
}

function sendForm() {
    console.log( "send form");
    $('#sendAuthorizationForm').click();
}