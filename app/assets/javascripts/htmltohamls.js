$(document).on('turbolinks:load', function() {

    var timer, delay = 1500;
    $('#htmlField').bind('keydown blur change', function(e) {
        clearTimeout(timer);
        timer = setTimeout(function() {
            if( $('#htmlField').val().length > 0 ) {
                convertHtml();
            }
        }, delay );
    });

    $('#htmlToHamlBug').click(function(){
        $('#new_htmltohaml').attr('action', '/html2haml_bug');
        convertHtml();
    });
});

function convertHtml() {
    $('#submitHtmlConverter').click();
}
