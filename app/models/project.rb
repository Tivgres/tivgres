class Project < ApplicationRecord

  default_scope { where(published: true) }
  scope :not_published, -> { unscoped.where(published: false) }
end
