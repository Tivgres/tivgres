class User < ApplicationRecord
  extend FriendlyId
  friendly_id :username, use: :slugged

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :confirmable

  default_scope { where(deleted: false) }
  scope :deleted, -> { unscoped.where(deleted: true) }

  has_many :comments, dependent: :delete_all
  has_many :likes, dependent: :delete_all
end
