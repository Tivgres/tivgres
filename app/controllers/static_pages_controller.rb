class StaticPagesController < ApplicationController

  def index; end

  def ideology; end

  def about; end

  def contact
    @contact = Contact.new
  end

  def contact_form; end

  def admin
    render(file: File.join(Rails.root, 'public/403.html'), status: 403, layout: false)
  end

end
