# frozen_string_literal: true
require 'base64'

class HtmltohamlsController < ApplicationController
  def index
    @html2haml = Htmltohaml.new
  end

  def html2haml
    require 'net/http'
    require 'json'
    @html2haml = Htmltohaml.new html2haml_params
    make_magic
    render :index
  end

  def bug
    @html2haml = Htmltohaml.new html2haml_params
    @html2haml.system = false
    if @html2haml.save
      flash[:success] = 'Thank you, bug was accepted'
    else
      flash[:danger] = 'Oooooops! Bug was not saved, try again please!'
    end
    redirect_to html2haml_path
  end

  private

  def make_magic
    resp = make_request
    if resp['success'] && resp['response_haml'].size > 0
      success_stat resp
      @html2haml.response_haml = resp['response_haml'].to_s
    else
      fail_stat resp
      @html2haml.response_haml = "Oops!
Check your HTML code for errors.
But really, if you think that he has no mistakes - click the \"BUG\" button at the bottom of this page."
    end
  rescue StandardError => e
    puts "failed #{e}"
    @html2haml.response_haml = "Oops! Server error"
  end

  def make_request
    req = Net::HTTP::Post.new('/',
                              'Content-Type': 'application/json',
                              'Authorization':
                                  Base64.encode64("#{ENV['HTML2HAML_USER']}:#{ENV['HTML2HAML_PASS']}"))
    req.body = @html2haml.source_html
    start_time = Time.now
    resp = Net::HTTP.new(ENV['HTML2HAML_SERVER'], ENV['HTML2HAML_PORT']).request(req)
    handle_time = (Time.now - start_time).in_milliseconds
    if response.code.to_i == 200
      JSON.parse(resp.body).merge!(handle_time: handle_time)
    else
      {}
    end
  end

  def success_stat(resp)
    Htmltohaml.create(lines_in: resp['lines_in'],
                      symbols_in: resp['symbols_in'],
                      lines_out: resp['lines_out'],
                      symbols_out: resp['symbols_out'],
                      handle_time: resp[:handle_time])
  end

  def fail_stat(resp)
    Htmltohaml.create(success: false,
                      lines_in: resp['lines_in'],
                      symbols_in: resp['symbols_in'],
                      error_body: resp['error_body'],
                      error_stack: resp['error_stack'],
                      source_html: @html2haml.source_html,
                      handle_time: resp[:handle_time])
  end

  def html2haml_params
    params.require(:htmltohaml).permit!
  end
end
