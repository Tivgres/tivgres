# frozen_string_literal: true

Rails.application.routes.draw do
  # resources :likes
  # resources :comments
  # resources :accounts
  # resources :libraries
  # resources :languages
  devise_for :users, controllers: {
    sessions: 'devise/sessions',
    registrations: 'devise/registrations'
  }
  devise_scope :user do
    get 'login', to: 'devise/sessions#new'
    post 'login', to: 'devise/sessions#create'
    get 'registration', to: 'devise/registrations#new'
    post 'registration', to: 'devise/registrations#create'
    delete 'logout', to: 'devise/sessions#destroy'
  end

  #html2haml
  get 'html2haml', action: :index, controller: 'htmltohamls'
  post 'html2haml', action: :html2haml, controller: 'htmltohamls'
  post 'html2haml_bug', action: :bug, controller: 'htmltohamls'

  # static pages
  get 'ideology', action: :ideology, controller: 'static_pages'
  get 'about', action: :about, controller: 'static_pages'
  get 'contact', action: :contact, controller: 'static_pages'
  post 'contact', action: :contact_form, controller: 'static_pages'
  get 'admin', action: :admin, controller: 'static_pages'

  resources :subscribes
  resources :projects
  root 'static_pages#index'
end
