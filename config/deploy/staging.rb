# frozen_string_literal: true

ENV_FILE_PATH = '.env.staging'.freeze

require 'dotenv'
Dotenv.load(ENV_FILE_PATH)

server ENV['VPS_HOST'],
       port: ENV['VPS_SSH_PORT'],
       user: ENV['VPS_USER'],
       roles: %w[app db web]

set :deploy_to, "~/#{ENV['VPS_WWW_PATH']}/#{ENV['APP_NAME']}"
set :shared_path, "#{fetch(:deploy_to)}/shared"
set :application, ENV['APP_NAME']
set :repo_url, ENV['GIT_REPO_URL']
set :branch, ENV['GIT_REPO_BRANCH']
set :stage, :production
set :rails_env, :production

set :ssh_options, keys: %w[~/.ssh/id_rsa]



namespace :deploy do

  task :create_symlink do
    on roles :app do
      within current_path do
        p '#' * 60
        p 'CREATING SYMLINK'
        p '#' * 60
        execute "ln -s ~/#{ENV['VPS_WWW_PATH']}/#{ENV['APP_NAME']}/shared/config/#{ENV_FILE_PATH} ~/#{ENV['VPS_WWW_PATH']}/#{ENV['APP_NAME']}/current"
      end
    end
  end

  task :restart do
    on roles :app do
      within current_path do
        p '#' * 60
        p 'REBOOTING SERVER'
        p '#' * 60
        execute "kill -SIGKILL `cat ~/#{ENV['VPS_WWW_PATH']}/#{ENV['APP_NAME']}/shared/tmp/pids/server.pid` && rm ~/#{ENV['VPS_WWW_PATH']}/#{ENV['APP_NAME']}/shared/tmp/pids/server.pid"
        execute :bundle, "exec rails s -e production -d -p #{ENV['RUN_SERVER_PORT']}"
      end
    end
  end

  task :seed do
    on roles :app do
      within current_path do
        execute :rake, 'db:seed'
      end
    end
  end

  # after :restart, :clear_cache do
  #   on roles(:web), in: :groups, limit: 3, wait: 10 do
  #     # Here we can do anything such as:
  #     within release_path do
  #       execute :rake, 'assets:clean'
  #     end
  #   end
  # end

end